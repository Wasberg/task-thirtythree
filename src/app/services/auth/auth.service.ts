import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/Models/user.model'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(user: User): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login` , {
      user: { ...user }
    }).toPromise();
  }

  register(user: User): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      user: { ...user }
    }).toPromise()
  }

}

