import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/pages/login-page/login-page.component';
import { RegisterComponent } from './components/pages/register-page/register.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'dashboard',
    loadChildren: ()=> import('./components/pages/dashboard-page/dashboard.module').then(m => m.DashboardModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'surveys',
    loadChildren: () => import('./components/surveys/survey-list/survey-list.module').then(m => m.SurveyListModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'surveys/:surveyId',
    loadChildren: () => import('./components/surveys/survey-detail/survey-detail.module').then(m => m.SurveyDetailModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
