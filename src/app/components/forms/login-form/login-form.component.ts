import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/Models/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

  loginForm: FormGroup = new FormGroup({ 
    //Username with validators
    username: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.email ]),
    //Password with validators
    password: new FormControl('', [ Validators.required, Validators.minLength(6), Validators.maxLength(30) ])
  });

  private loginResult: any = {
    message: ''
  };

  isLoading: boolean = false;

  constructor(private authService: AuthService, private session: SessionService, private router: Router) { 
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard')
    }
  }

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  async onLoginClicked() {

      // tries connection to API
      try {
        this.isLoading = true;
        const result: any = await this.authService.login(this.loginForm.value);
        // saves session and redirect to dashboard
        if ( result.status < 400) {
          this.session.save( { token: result.data.token, username: result.data.user.username } )
          this.router.navigateByUrl('/dashboard');
        }

      } catch (e) {
        // Shows you what went wrong and reset form.
        this.isLoading = false;
        //sends message to alert
        this.loginResult.message = "Something went wrong: " + e.error.error;
        //reset form
        this.loginForm.reset('username');
        this.loginForm.reset('password');
        //Alert you with what is wrong.
        this.loginAttempt.emit(this.loginResult);
      }

    
  }



  ngOnInit(): void {
  }

}
