import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../../Models/user.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter();

  registerForm: FormGroup = new FormGroup({
    //Username with validators
    username: new FormControl('', [Validators.required, Validators.minLength(2), Validators.email]),
    //Password with validators
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(30)]),
    // confirm Password
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(30)])
  });

  // private messages
  private registerResult: any = {
    message: ''
  };
  
  isLoading: boolean = false;

  constructor(private authService: AuthService, private router: Router, private session: SessionService) { 
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard')
    }
  }

  get username() {
    return this.registerForm.get('username')
  }

  get password() {
    return this.registerForm.get('password')
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword')
  }

  //Checks missmatch when passwords inputboxes change.
  onPasswordChange() {
    if (this.confirmPassword.value == this.password.value) {
      this.confirmPassword.setErrors(null);
    } else {
      this.confirmPassword.setErrors({ mismatch: true });
    }
  }

  async onRegisterClicked() {

    try {
      //Register succeded
      this.isLoading = true;

      const result: any = await this.authService.register(this.registerForm.value);

      console.log(result);

      if ( result.status < 400) {
        this.session.save( { token: result.data.token, username: result.data.user.username } )
        this.router.navigateByUrl('/dashboard');
      }

    } catch (e) {
      console.error(e);
      // Shows you what problem you encountered and resets from
      this.registerResult.message = "You encountered a problem creating your account: " + e.error.error;
      this.registerForm.reset('username');
      this.registerForm.reset('password');
      this.registerForm.reset('confirmPassword');
      // sends outcome to alert.
      this.registerAttempt.emit(this.registerResult);
    } finally {
      this.isLoading = false;
    }

  }

ngOnInit(): void {
}

}
